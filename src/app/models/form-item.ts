export interface FormItem {
  name: string;
  text: string;
  icon: string[];
  type: "dropDown" | "radioGroup" | "textBox";
}
