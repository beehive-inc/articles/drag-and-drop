import {Component} from "@angular/core";
import {CdkDragDrop, copyArrayItem, moveItemInArray} from "@angular/cdk/drag-drop";
import {FormItem} from "../models/form-item";

@Component({
  selector: "form-container",
  template: `
    <div cdkDropList
         class="form-container"
         (cdkDropListDropped)="onItemDropped($event)"
         [cdkDropListData]="currentFormItems">
      <div class="form-container-item" *ngFor="let item of currentFormItems" cdkDrag>
        <ng-container dynamicRender [config]="item"></ng-container>
      </div>
    </div>
  `,
  styleUrls: ["./form-container.component.scss"]
})
export class FormContainerComponent {
  public currentFormItems: Array<FormItem> = [];

  public onItemDropped(event: CdkDragDrop<any>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      copyArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
      console.log("container item", event.item);
    }
  }
}
