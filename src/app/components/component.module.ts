import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormItemListComponent} from "./form-item-list.component";
import {FormItemComponent} from "./form-item.component";
import {FormContainerComponent} from "./form-container.component";
import {FormContainerRowComponent} from "./form-container-row.component";
import {FormBuilderComponent} from "./form-builder.component";
import {TextBoxComponent} from "./form/text-box.component";
import {DropDownComponent} from "./form/drop-down.component";
import {RadioGroupComponent} from "./form/radio-group.component";
import {DragDropModule} from "@angular/cdk/drag-drop";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {DynamicRenderDirective} from "../directives/dynamic-render.directive";
import {BrowserModule} from "@angular/platform-browser";

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    DragDropModule,
    FontAwesomeModule
  ],
  exports: [
    FormBuilderComponent
  ],
  declarations: [
    FormItemListComponent,
    FormItemComponent,
    FormContainerComponent,
    FormContainerRowComponent,
    FormBuilderComponent,

    TextBoxComponent,
    DropDownComponent,
    RadioGroupComponent,

    DynamicRenderDirective
  ],
  entryComponents: [
    TextBoxComponent,
    DropDownComponent,
    RadioGroupComponent
  ]
})
export class ComponentModule {}
