import {Component, Input} from "@angular/core";
import {FormItem} from "../models/form-item";

@Component({
  selector: "form-item-list",
  template: `
    <div class="form-item-list" cdkDropList [cdkDropListData]="formItems" cdkDropListSortingDisabled>
      <form-item *ngFor="let formItem of formItems" [formItem]="formItem"></form-item>
    </div>
  `,
  styleUrls: ["./form-item-list.component.scss"]
})
export class FormItemListComponent {
  @Input() public formItems: Array<FormItem>;
}
