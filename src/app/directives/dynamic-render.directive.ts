import {ComponentFactory, ComponentFactoryResolver, Directive, Input, OnInit, ViewContainerRef} from "@angular/core";
import {FormItem} from "../models/form-item";
import {DropDownComponent} from "../components/form/drop-down.component";
import {RadioGroupComponent} from "../components/form/radio-group.component";
import {TextBoxComponent} from "../components/form/text-box.component";

export type ComponentType = DropDownComponent | RadioGroupComponent | TextBoxComponent;

@Directive({
  selector: "[dynamicRender]"
})
export class DynamicRenderDirective implements OnInit {
  @Input() config: FormItem;

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private viewContainerRef: ViewContainerRef) {
  }

  public ngOnInit() {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.getComponent(this.config.type));
    this.viewContainerRef.clear();
    const componentRef = this.createComponent(this.config.type, componentFactory);
  }

  private getComponent(componentType: string) {
    switch (componentType) {
      case "dropDown":
        return DropDownComponent;
      case "radioGroup":
        return RadioGroupComponent;
      case "textBox":
        return TextBoxComponent;
    }
  }

  private createComponent(componentType: string, componentFactory: ComponentFactory<ComponentType>) {
    switch (componentType) {
      case "dropDown":
        return this.viewContainerRef.createComponent<DropDownComponent>(componentFactory);
      case "radioGroup":
        return this.viewContainerRef.createComponent<RadioGroupComponent>(componentFactory);
      case "textBox":
        return this.viewContainerRef.createComponent<TextBoxComponent>(componentFactory);
    }
  }
}
