import {Component} from "@angular/core";

@Component({
  selector: "drop-down",
  template: `
    <div>
      <div>Drop Down Label</div>
      <div>
        <select>
          <option>Option 1</option>
          <option>Option 2</option>
        </select>
      </div>
    </div>
  `
})
export class DropDownComponent {

}
