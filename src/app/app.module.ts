import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import {HomeComponent} from "./pages/home.component";
import {DragDropModule} from "@angular/cdk/drag-drop";
import {CommonModule} from "@angular/common";
import {BuilderComponent} from "./pages/builder.component";
import {ComponentModule} from "./components/component.module";
import {FaIconLibrary} from "@fortawesome/angular-fontawesome";
import {faTextHeight} from "@fortawesome/free-solid-svg-icons/faTextHeight";
import {faChevronDown} from "@fortawesome/free-solid-svg-icons/faChevronDown";
import {faDotCircle} from "@fortawesome/free-solid-svg-icons/faDotCircle";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BuilderComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    DragDropModule,
    AppRoutingModule,
    ComponentModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTextHeight, faChevronDown, faDotCircle);
  }
}
