import {Component} from "@angular/core";
import {FormItem} from "../models/form-item";

@Component({
  selector: "form-builder",
  template: `
    <div class="form-builder" cdkDropListGroup>
      <div class="form-items">
        <div class="container">
          <form-item-list [formItems]="formItems"></form-item-list>
        </div>
      </div>
      <div class="form-container">
        <div class="container">
          <form-container></form-container>
        </div>
      </div>
    </div>
  `,
  styleUrls: ["./form-builder.component.scss"]
})
export class FormBuilderComponent {
  public formItems: Array<FormItem> = [
    {
      name: "textBox",
      text: "Text Box",
      icon: ["fas", "text-height"],
      type: "textBox"
    },
    {
      name: "dropDown",
      text: "Drop Down",
      icon: ["fas", "chevron-down"],
      type: "dropDown"
    },
    {
      name: "radioGroup",
      text: "Radio Group",
      icon: ["fas", "dot-circle"],
      type: "radioGroup"
    }
  ];
}
