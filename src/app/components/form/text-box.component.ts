import {Component} from "@angular/core";

@Component({
  selector: "text-box",
  template: `
    <div>
      <div>Text Label</div>
      <div><input type="text" /></div>
    </div>
  `
})
export class TextBoxComponent {

}
