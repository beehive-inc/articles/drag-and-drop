import {Component} from "@angular/core";
import {CdkDragDrop, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";

@Component({
  selector: "home-page",
  template: `
    <div class="list" cdkDropListGroup>
      <div class="list-col">
        <h2>List One</h2>
        <div cdkDropList [cdkDropListData]="listOne" (cdkDropListDropped)="onItemDropped($event)">
          <div class="list-item" *ngFor="let item of listOne" cdkDrag>{{item.text}}</div>
        </div>
      </div>
      <div class="list-col">
        <h2>List Two</h2>
        <div cdkDropList [cdkDropListData]="listTwo" (cdkDropListDropped)="onItemDropped($event)">
          <div class="list-item" *ngFor="let item of listTwo" cdkDrag>{{item.text}}</div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent {
  public listOne = [
    { id: 1, text: "Item One List One" },
    { id: 2, text: "Item Two List One" },
    { id: 3, text: "Item Three List One" },
    { id: 4, text: "Item Four List One" }
  ];

  public listTwo = [
    { id: 5, text: "Item One List Two" },
    { id: 6, text: "Item Two List Two" },
    { id: 7, text: "Item Three List Two" },
    { id: 8, text: "Item Four List Two" }
  ];

  public onItemDropped(event: CdkDragDrop<any>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
    }
  }
}
