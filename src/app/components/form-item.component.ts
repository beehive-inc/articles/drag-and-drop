import {Component, Input} from "@angular/core";
import {FormItem} from "../models/form-item";

@Component({
  selector: "form-item",
  template: `
    <div cdkDrag class="drag-item" [cdkDragData]="formItem">
      <span class="icon"><fa-icon [icon]="formItem.icon"></fa-icon></span>
      <span>{{formItem.text}}</span>
    </div>
  `,
  styleUrls: ["./form-item.component.scss"]
})
export class FormItemComponent {
  @Input() public formItem: FormItem;
}
