import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {HomeComponent} from "./pages/home.component";
import {BuilderComponent} from "./pages/builder.component";


const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
    path: "builder",
    component: BuilderComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
