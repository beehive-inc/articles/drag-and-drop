import {Component} from "@angular/core";

@Component({
  selector: "builder-page",
  template: `
    <form-builder></form-builder>
  `
})
export class BuilderComponent {

}
