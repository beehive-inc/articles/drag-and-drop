import {Component} from "@angular/core";

@Component({
  selector: "radio-group",
  template: `
    <div>
      <input type="radio" name="itemGroup" id="item1" value="value1"/><label for="item1">Item 1</label>
      <input type="radio" name="itemGroup" id="item2" value="value2"/><label for="item2">Item 2</label>
      <input type="radio" name="itemGroup" id="item3" value="value3"/><label for="item3">Item 3</label>
    </div>
  `
})
export class RadioGroupComponent {

}
